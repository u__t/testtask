package com.task.test.testtask.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.Parse;
import com.task.test.testtask.R;
import com.task.test.testtask.data.ElementControl;
import com.task.test.testtask.fragments.FavouriteFragment;
import com.task.test.testtask.fragments.MainFragment;
import com.task.test.testtask.interfaces.Observer;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Observer {

    private Toolbar mToolbar;
    private NavigationView mNavigationView;
    private ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);



        if(null == savedInstanceState){
            Parse.enableLocalDatastore(this);
            Parse.initialize(this, "mSHOqCPQE2sR8VIeSgo8qCyibm8DLHa0Z1ufkD6P",
                    "yUw5l97kMS2sDRmVAIB0Sorq1z7G5VZ8XWB6YXEs");

            ElementControl.getInstance().getDataFromParse();
            ElementControl.getInstance().addObserver(this);

            setFragment(R.id.main);

            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage(this.getString(R.string.loading_text));
            mProgressDialog.show();
        }
    }

    @Override
    public void onPause(){
        Log.d("myLog", "onPause");
        ElementControl.getInstance().save();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setFragment(int id){
        Fragment objFragment;
        String title;
        switch(id){
            case R.id.main:
                objFragment = new MainFragment();
                title = getString(R.string.main_tab_text);
                break;
            case R.id.favourites:
                objFragment = new FavouriteFragment();
                title = getString(R.string.favourites_tab_text);
                break;
            default:
                objFragment = new MainFragment();
                title = getString(R.string.main_tab_text);
                break;
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, objFragment);
        ft.commit();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        setFragment(item.getItemId());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void endLoadElements(boolean isOk){
        mProgressDialog.dismiss();
        if(isOk){
            mNavigationView.getMenu().getItem(0).setChecked(true);
            mToolbar.setTitle(getString(R.string.main_tab_text));
        }
    }
}
