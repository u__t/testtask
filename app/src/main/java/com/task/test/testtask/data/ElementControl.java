package com.task.test.testtask.data;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.task.test.testtask.interfaces.Observable;
import com.task.test.testtask.interfaces.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10.11.2015.
 *
 */
public class ElementControl implements Observable {

    private ArrayList<Element> mElementsList = null;
    private ArrayList<Element> mFavouritesList = null;
    private boolean mIsMustUpdate = false;//true если что-нибудь изменилось (надо сохраить)
    private ArrayList<Observer> mObserversList = null;

    private static ElementControl sOurInstance = new ElementControl();
    public static ElementControl getInstance() {
        return sOurInstance;
    }
    private ElementControl() {
        mElementsList = new ArrayList<Element>();
        mFavouritesList = new ArrayList<Element>();
        mObserversList = new ArrayList<Observer>();
    }

    public ArrayList<Element> getElements(){
        return mElementsList;
    }
    public ArrayList<Element> getFavourite(){
        return mFavouritesList;
    }

    public final Element getElement(int tab, int position){
        switch (tab){
            case 0: return mElementsList.get(position);
            case 1: return mFavouritesList.get(position);
            default:return mElementsList.get(position);
        }
    }

    private void makeFavourites(){
        Log.d("myLog", "makeFavourites");
        mFavouritesList.clear();
        for (Element elem : mElementsList) {
            if(elem.getIsFavourite()){
                mFavouritesList.add(elem);
            }
        }
    }

    public void addToFavourite(int position){
        Element elem = mElementsList.get(position);
        elem.setFavourite(true);
        mFavouritesList.add(elem);
        mIsMustUpdate = true;
    }
    public void removeFromFavourite(int position){
        Element elem = mFavouritesList.get(position);
        elem.setFavourite(false);
        mFavouritesList.remove(position);
        mIsMustUpdate = true;
    }

    public void save(){
        if(mIsMustUpdate){
            Log.d("myLog", "Save");
            for (Element elem: mElementsList) {
                elem.save();
            }
            mIsMustUpdate = false;
        }else{
            Log.d("myLog", "Not save");
        }
    }

    public void getDataFromParse(){
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("DataObject");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    for (ParseObject object : list) {
                        Log.d("myLog", "element read");
                        Element newElem = new Element(object);
                        mElementsList.add(newElem);
                    }
                    makeFavourites();
                    notifyObservers(true);
                } else {
                    Log.d("myLog", "Parse error: " + e.toString());
                    notifyObservers(false);
                }
            }
        });
    }

    @Override
    public void addObserver(Observer obj){
        mObserversList.add(obj);
        Log.d("myLog", "some thing add");
    }

    @Override
    public void notifyObservers(boolean isOk){
        for (Observer obj: mObserversList) {
            obj.endLoadElements(isOk);
            Log.d("myLog", "send");
        }
    }
}
