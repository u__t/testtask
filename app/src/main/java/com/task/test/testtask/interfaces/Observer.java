package com.task.test.testtask.interfaces;

/**
 * Created by user on 13.11.2015.
 *
 */
public interface Observer {
    void endLoadElements(boolean isOk);
}
