package com.task.test.testtask.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.task.test.testtask.R;
import com.task.test.testtask.adapter.RecyclerItemClickListener;
import com.task.test.testtask.adapter.RecyclerViewAdapter;
import com.task.test.testtask.data.ElementControl;
import com.task.test.testtask.interfaces.Observer;

/**
 * parent class for main and favourite fragments
 * Created by user on 13.11.2015.
 */
abstract public class RecyclerViewFragment extends Fragment implements Observer {

    protected RecyclerViewAdapter mAdapter;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState){
        Log.d("myLog", "onCreate");
        if(null == savedInstanceState){
            ElementControl.getInstance().addObserver(this);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("myLog", "onCronCreateView");
        View rootView =  inflater.inflate(R.layout.fragment_layout, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.var_list_view);

        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
        registerForContextMenu(mRecyclerView);

        setAdapterWithRightList();


        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), mRecyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                clickHandler(view, position);
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {
                                longClickHandler(view, position);
                            }
                        })
        );

        return rootView;
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void endLoadElements(boolean isOk){
        if(isOk)Log.d("myLog", "update");
        else Log.d("myLog", "not update");
        if(isOk) mAdapter.notifyDataSetChanged();
    }

    abstract public void setAdapterWithRightList();
    abstract public void clickHandler(View view, int position);
    abstract public void longClickHandler(View view, int position);
    


}
