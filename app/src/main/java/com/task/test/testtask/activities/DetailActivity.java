package com.task.test.testtask.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.task.test.testtask.R;
import com.task.test.testtask.data.Element;
import com.task.test.testtask.data.ElementControl;

/**
 * Created by user on 10.11.2015.
 *
 */
public class DetailActivity extends AppCompatActivity {

    private Element mCurrentElement = null;
    private int mPositionInList = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        if(null != getActionBar()){
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mPositionInList = getIntent().getIntExtra("element_position", 0);
        int listNumber = getIntent().getIntExtra("list_num", 0);
        mCurrentElement = ElementControl.getInstance().getElement(listNumber, mPositionInList);

        ((TextView) findViewById(R.id.name_label)).setText(mCurrentElement.getName());
        ((TextView) findViewById(R.id.description_label)).setText(
                mCurrentElement.getFullDescription());
        ((ImageView)findViewById(R.id.image_id)).setImageBitmap(mCurrentElement.getImgBitMap());

        RadioButton favourBtn = ((RadioButton) findViewById(R.id.switch_to_favour));
        favourBtn.setChecked(mCurrentElement.getIsFavourite());
        favourBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCurrentElement.getIsFavourite()) {
                    //Если уже в избранных, то ни чего не делать
                    ElementControl.getInstance().addToFavourite(mPositionInList);
                }
                ((RadioButton) v).setChecked(mCurrentElement.getIsFavourite());

            }
        });
    }

    @Override
    public void onPause(){
        ElementControl.getInstance().save();
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
