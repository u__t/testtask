package com.task.test.testtask.interfaces;

import android.graphics.Bitmap;

/**
 * Created by user on 14.11.2015.
 * delegate for set image bitmap to imageView after it load
 */
public interface ImageLoadListener {
    void updateImage(Bitmap img);
}
