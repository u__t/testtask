package com.task.test.testtask.adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.task.test.testtask.R;
import com.task.test.testtask.data.Element;
import com.task.test.testtask.interfaces.ImageLoadListener;

import java.util.ArrayList;

/**
 * Created by user on 11.11.2015.
 *
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private ArrayList<Element> mCurrentList;

    public RecyclerViewAdapter(ArrayList<Element> _elements) {
        this.mCurrentList = _elements;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_element,
                viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Element elem = mCurrentList.get(i);
        viewHolder.name.setText(elem.getName());
        viewHolder.shortDesc.setText(elem.getShortDescription());
        viewHolder.icon.setImageBitmap(elem.getImgBitMap());
        //elem.setImageViewInMain(viewHolder.icon);
        elem.setImageLoadListener(viewHolder);
    }

    @Override
    public int getItemCount() {
        return mCurrentList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements ImageLoadListener {
        private TextView name;
        private ImageView icon;
        private TextView shortDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.header);
            icon = (ImageView) itemView.findViewById(R.id.icon_id);
            shortDesc = (TextView) itemView.findViewById(R.id.shortDesc);
        }

        @Override
        public void updateImage(Bitmap img){
            icon.setImageBitmap(img);
        }
    }
}
