package com.task.test.testtask.fragments;

import android.content.Intent;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.task.test.testtask.R;
import com.task.test.testtask.activities.DetailActivity;
import com.task.test.testtask.adapter.RecyclerViewAdapter;
import com.task.test.testtask.data.ElementControl;

/**
 * Created by user on 13.11.2015.
 *
 */
public class FavouriteFragment extends RecyclerViewFragment {

    private ActionMode mActionMode = null;
    private int mLongTabItemPos = -1;

    @Override
    public void setAdapterWithRightList(){
        mAdapter = new RecyclerViewAdapter(ElementControl.getInstance().getFavourite());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
     public void clickHandler(View view, int position){
        Log.d("myLog", "favourite click on " + String.valueOf(position));

        Intent elemIntent = new Intent(view.getContext(),
                DetailActivity.class);
        elemIntent.putExtra("element_position", position);
        elemIntent.putExtra("list_num", 1);
        startActivity(elemIntent);
    }

    @Override
    public void longClickHandler(View view, int position){
        Log.d("myLog", "long click on " + String.valueOf(position));
        if (mActionMode == null) {
            mLongTabItemPos = position;
            mActionMode = getActivity().startActionMode(mActionModeCallback);
            view.setSelected(true);
        }
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.context_item_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.remove_btn:
                    ElementControl.getInstance().removeFromFavourite(mLongTabItemPos);
                    mAdapter.notifyDataSetChanged();
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };
}
