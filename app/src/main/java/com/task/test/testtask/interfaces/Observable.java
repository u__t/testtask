package com.task.test.testtask.interfaces;

/**
 * Created by user on 13.11.2015.
 *
 */
public interface Observable {
    void addObserver(Observer obj);
    void notifyObservers(boolean isOk);
}
