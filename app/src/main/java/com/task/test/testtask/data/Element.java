package com.task.test.testtask.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.task.test.testtask.interfaces.ImageLoadListener;

import java.io.ByteArrayOutputStream;

/**
 * Created by user on 10.11.2015.
 *
 */
public class Element {
    private String mName;
    private String mShortDescription;
    private String mFullDescription;
    private boolean mIsFavourite;
    private Bitmap mImgBitMap;
    private String mParseObjectId;
    private ImageLoadListener mImageLoadListener;

    Element(ParseObject object){
        parsParseObject(object);
    }

    public String getName(){
        return mName;
    }
    public String getShortDescription(){
        return mShortDescription;
    }
    public String getFullDescription(){
        return mFullDescription;
    }

    public void setFavourite(boolean _isFavourite){
        mIsFavourite = _isFavourite;
    }
    public boolean getIsFavourite(){
        return mIsFavourite;
    }

    public Bitmap getImgBitMap(){
        return mImgBitMap;
    }

    /*public void setImageViewInMain(ImageView imageView){
        mImageViewInMain = imageView;
    }*/
    public void setImageLoadListener(ImageLoadListener imageLoadListener){
        mImageLoadListener = imageLoadListener;
    }

    private void parsParseObject(ParseObject object){
        mName = object.getString("name");
        mShortDescription = object.getString("short_description");
        mFullDescription = object.getString("full_description");
        mIsFavourite = object.getBoolean("in_favourite");
        mParseObjectId = object.getObjectId();
        ParseFile imageParseFile = object.getParseFile("image");
        if(imageParseFile != null){
            imageParseFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    if (e == null) {
                        mImgBitMap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        updateImage();
                    } else {
                        Log.d("myLog", "error in reading image: " + e.toString());
                    }
                }
            });
        }else{
            Log.d("myLog", "image file not found");
        }
    }

    private ParseObject makeParseObject(){
        ParseObject result =  ParseObject.createWithoutData("DataObject", mParseObjectId);
        result.put("name",mName);
        result.put("short_description",mShortDescription);
        result.put("full_description",mFullDescription);
        result.put("in_favourite",mIsFavourite);
        if(null != mImgBitMap) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            mImgBitMap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] b = baos.toByteArray();
            ParseFile imgFile = new ParseFile("default_bike.png", b);
            result.put("image", imgFile);
        }
        return result;
    }

    public void save(){
        makeParseObject().saveInBackground();
    }

    private void updateImage(){
        if(null != mImageLoadListener)
            mImageLoadListener.updateImage(mImgBitMap);
    }
}
