package com.task.test.testtask.fragments;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.task.test.testtask.activities.DetailActivity;
import com.task.test.testtask.adapter.RecyclerViewAdapter;
import com.task.test.testtask.data.ElementControl;

/**
 * Created by user on 13.11.2015.
 *
 */
public class MainFragment extends RecyclerViewFragment {
    @Override
    public void setAdapterWithRightList(){
        mAdapter = new RecyclerViewAdapter(ElementControl.getInstance().getElements());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void clickHandler(View view, int position){
        Log.d("myLog", "main click on " + String.valueOf(position));

        Intent elemIntent = new Intent(view.getContext(),
                DetailActivity.class);
        elemIntent.putExtra("element_position", position);
        elemIntent.putExtra("list_num", 0);
        startActivity(elemIntent);
    }

    @Override
    public void longClickHandler(View view, int position){}
}
